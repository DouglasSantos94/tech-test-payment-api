using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.Annotations;

namespace tech_test_payment_api.Models
{
    [PrimaryKey(nameof(Id))]
    public class Venda
    {
        public int Id { get; set; }
        
        public DateTime Data { get; set; }

        public string StatusVenda { get; set; }
        
        public ICollection<string> ItensVendidos { get; set; }

        public Vendedor vendedor { get; set; }
        
    }
}