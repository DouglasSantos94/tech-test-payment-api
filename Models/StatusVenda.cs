namespace tech_test_payment_api.Models
{
    public class StatusVenda
    {
        public static readonly string AguardandoPagamento = "Aguardando Pagamento";
        public static readonly string PagamentoAprovado = "Pagamento Aprovado";
        public static readonly string EnviadoParaTransportadora = "Enviado Para Transportadora";
        public static readonly string Entregue = "Entregue";
        public static readonly string Cancelada = "Cancelada";
    }
}