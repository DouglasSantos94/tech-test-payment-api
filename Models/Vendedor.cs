using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;


namespace tech_test_payment_api.Models
{
    [PrimaryKey(nameof(Id))]
    public class Vendedor : Pessoa
    {
        public Vendedor(int id, string cpf, string nome, string email, string telefone) : base(id, cpf, nome, email, telefone)
        {

        }

        public Vendedor()
        {
            
        }
    }
}