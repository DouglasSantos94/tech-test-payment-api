using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace tech_test_payment_api.Models
{
    public class Pessoa
    {
        public Pessoa(int id, string cpf, string nome, string email, string telefone)
        {
            Id = id;
            Cpf = cpf;
            Nome = nome;
            Email = email;
            Telefone = telefone;
        }     

        public Pessoa()
        {
            
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Cpf { get; set; }

        public string Nome { get; set; }

        public string Email { get; set; }

        public string Telefone { get; set; }

   
    }
}