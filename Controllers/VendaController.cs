using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;
using tech_test_payment_api.Context;
using Microsoft.EntityFrameworkCore;

namespace tech_test_payment_api.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class VendaController : ControllerBase
    {

        private readonly LojaContext db;

        public VendaController(LojaContext db)
        {
            this.db = db;
        }


        /// <summary>
        /// Lista todas as vendas cadastradas.
        /// </summary>
        /// <returns>Vendas cadastradas</returns>
        /// <response code="200">Retorna as vendas cadastradas</response>
        [HttpGet]
        public async Task<IActionResult> ObterTodasAsVendas()
        {
            var vendas = await db.Vendas.Include(v => v.vendedor).ToArrayAsync();
            var response = vendas.Select(v => new {
                id = v.Id,
                data = v.Data,
                statusVenda = v.StatusVenda,
                itensVendidos = v.ItensVendidos,
                vendedor = v.vendedor
            });
            return Ok(response);
        }

        /// <summary>
        /// Busca de uma venda pelo Id.
        /// </summary>
        /// <returns>Venda buscada pelo id</returns>
        /// <response code="200">Retorna uma venda cadastrada.</response>
        [HttpGet("{id}")]
        public IActionResult BuscarVenda(int id)
        {
            try 
            {
                var venda = db.Vendas.Include(v => v.vendedor).Single(v => v.Id == id);

                return Ok(venda);
            } 
            catch(InvalidOperationException)
            {
                throw new InvalidOperationException("Venda não encontrada!");
            }
        }

        /// <summary>
        /// Cadastro de uma venda.
        /// </summary>
        /// <response code="200">Retorna a venda criada</response>
        [HttpPost]
        public async Task<IActionResult> RegistrarVenda([FromBody]Venda venda)
        {
            if(venda.StatusVenda != StatusVenda.AguardandoPagamento)
                throw new ArgumentException("O status deve iniciar em Aguardando Pagamento!");
            
            if(venda.ItensVendidos.Count == 0)
                throw new ArgumentException("A venda deve conter pelo menos um produto!");
    
            db.Vendas.Add(venda);
            await db.SaveChangesAsync();
            return Ok(venda);
        }

        /// <summary>
        /// Atualização de uma venda.
        /// </summary>
        /// <response code="200">Retorna a venda atualizada</response>
        [HttpPut]
        [Route("~/api/AtualizarVenda")]
        public async Task<IActionResult> AtualizarVenda([FromBody]Venda venda)
        {
            try
            {
                var vendaBanco = db.Vendas.Include(v => v.vendedor).Single(v => v.Id == venda.Id);
                switch(vendaBanco.StatusVenda)
                {
                    case "Aguardando Pagamento":
                        if((venda.StatusVenda) != StatusVenda.PagamentoAprovado && (venda.StatusVenda) != StatusVenda.Cancelada)
                            throw new ArgumentException("Alteração de status inválida!");
                        break;
                    case "Pagamento Aprovado":
                        if((venda.StatusVenda) != StatusVenda.EnviadoParaTransportadora && ((venda.StatusVenda) != StatusVenda.Cancelada))
                            throw new ArgumentException("Alteração de status inválida!");
                        break;
                    case "Enviado Para Transportadora":
                        if(venda.StatusVenda != StatusVenda.Entregue)
                            throw new ArgumentException("Alteração de status inválida!");
                        break;
                    default:
                        throw new ArgumentException("Alteração de status inválida!");
                }
                vendaBanco.StatusVenda = venda.StatusVenda;

                await db.SaveChangesAsync();
                
                
                return Ok(vendaBanco);
            } 
            catch (InvalidOperationException)
            {
                throw new InvalidOperationException("Venda não existe no banco!");
            }
     
        }
    }
}