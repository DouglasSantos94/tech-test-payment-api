## Teste técnico final do Bootcamp Pottencial .NET Developer

Olá! Este projeto foi feito como conclusão para o Bootcamp Pottencial .NET Developer.

O projeto consiste em uma API desenvolvida em .NET Core, que permite gerenciar um sistema de Vendas.

Para testar o projeto, por gentileza, siga os passos a seguir:

1 - Caso não tenha instalado o .NET no seu computador, instale seguindo o link: https://dotnet.microsoft.com/en-us/download/dotnet/6.0;

2 - Faça o clone do projeto na sua máquina;

3 - Apos clonado, navegue dentro do repositório tech_test_payment_api, e execute o comando dotnet watch run, para rodar o banco de dados InMemory e também abrir o Swagger;

4 - Para testar as requisições, é possível utilizar o Swagger, assim como o Postman.

5 - Oara testar no Postman, basta utilizar a url http://localhost:7003/rota_desejada.
